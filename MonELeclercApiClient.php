<?php

use GuzzleHttp\Client;

class MonELeclercApiClient
{
    const ENCRYPTION_KEY = "NEY0OUM4OEM2RkU4NDQx9zg3NjJFOEIx";

    private Client $client;

    private $token = null;
    private $shopNumber = null;

    private $cardNumber = null;

    public function __construct($token = null, $baseUri = "https://hpcs.mservices.eu", $timeout = 30) {
        $this->token = $token;

        $this->client = new \GuzzleHttp\Client([
            "base_uri" => $baseUri,
            "timeout" => $timeout
        ]);
    }

    // Courtesy of Andrew Moore: https://www.php.net/manual/en/function.uniqid.php#94959
    public static function generateUuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    private static function generateNoVerification($dateTime, $cardNumber = null) {
        $string = $dateTime->format("YmdHisv");

        if (!empty($cardNumber)) {
            $string = "{$cardNumber}{$string}";
        }

        return base64_encode(hash("sha512", $string, true));
    }

    private function getAuthenticationData() {
        $currentDateTime = new \DateTime();

        return [
            "JETON" => $this->token,
            "DATE_REQUETE" => $currentDateTime->format('Y-m-d\TH:i:s\.v\Z'),
            "NO_VERIFICATION" => self::generateNoVerification($currentDateTime, $this->cardNumber)
        ];
    }

    public function post($path, $data, $addAuthentication = true) {
        if ($addAuthentication) {
            $data = array_merge($data, $this->getAuthenticationData());
        }

        $response = $this->client->post($path, [
            "json" => $data
        ]);

        return $this->decodeResponse($response);
    }

    private function decodeJsonResponse(\Psr\Http\Message\ResponseInterface $response) {
        $responseJson = json_decode($response->getBody(), true);

        if ($responseJson === NULL) {
            throw new Exception("Response decoding failed: " . json_last_error_msg());
        }

        if (!isset($responseJson['CR'])) {
            throw new Exception("Response invalid.");
        }

        $returnCode = intval($responseJson['CR']);

        if ($returnCode !== 0) {
            switch($returnCode) {
                case 60:
                    throw new Exception("Error 60");
                case 80:
                    throw new Exception("Error 80");
                default:
                    throw new Exception("Unknown return code: {$returnCode}");
            }
        }

        return $responseJson;
    }

    private function decodeResponse(\Psr\Http\Message\ResponseInterface $response) {
        if ($response->getStatusCode() != 200) {
            throw new Exception("Invalid status code: {$response->getStatusCode()}");
        }

        if (!$response->hasHeader("Content-Type")) {
            throw new Exception("No 'Content-Type' header returned.");
        }

        $contentType = explode(";", $response->getHeaderLine("Content-Type"))[0];

        switch($contentType) {
            case 'application/json':
                return $this->decodeJsonResponse($response);
            default:
                throw new Exception("Unknown 'Content-Type': " . $response->getHeaderLine("Content-Type"));
        }
    }

    public function authenticateDevice($shopNumber, $deviceUuid, $deviceModel = "JAT-L29", $systemVersion = "9", $clientVersion = "5.0.1") {
        $data = [
            "NO_MACHINE" => $deviceUuid,
            "CODE_SYSTEME" => "A",
            "CODE_MODELE" => $deviceModel,
            "NO_VERSION_SYSTEME" => $systemVersion,
            "NO_VERSION_CLIENT" => $clientVersion,
            "NO_MAGASIN" => $shopNumber,
            "NO_CARTE_FIDELITE" => null,
            "LISTE_REFERENTIEL" => [
                "MAGASINS" => null,
                "SERVICES" => [
                    "CRC" => 0,
                    "ENVOYER_DONNEES" => true,
                    "TIMESTAMP_DONNEE" => null
                ],
                "MON_MAGASIN" => [
                    "CRC" => 0,
                    "ENVOYER_DONNEES" => true,
                    "TIMESTAMP_DONNEE" => null
                ],
                "PROSPECTUS" => null,
                "CARTE_FIDELITE_V2" => [
                    "CRC" => 0,
                    "ENVOYER_DONNEES" => true,
                    "TIMESTAMP_DONNEE" => null
                ],
                "BANNIERE" => null,
                "BANNIERE_JEU" => null,
                "DOCUMENT" => [
                    "CRC" => 0,
                    "ENVOYER_DONNEES" => true,
                    "TIMESTAMP_DONNEE" => null
                ],
                "VERSION" => [
                    "CRC" => 0,
                    "ENVOYER_DONNEES" => true,
                    "TIMESTAMP_DONNEE" => null
                ],
                "BON_D_ACHAT" => null,
            ],
            "JETON" => null,
            "CRC" => 0
        ];

        $data = array_merge($data, $this->getAuthenticationData());

        $iv = random_bytes(16);
        $encryptedData = openssl_encrypt(json_encode($data), "aes-192-cbc", base64_decode(self::ENCRYPTION_KEY), 0, $iv);

        $data = [
            "CRC" => 0,
            "DATA" => $encryptedData,
            "IV" => base64_encode($iv)
        ];

        $responseData = $this->post("/Mobile/api/Authentification/Authentifier", $data, true);

        if(isset($responseData['JETON'])) {
            $this->token = $responseData['JETON'];
        }

        if (isset($responseData['NO_MAGASIN_REFERENCE'])) {
            $this->shopNumber = $responseData['NO_MAGASIN_REFERENCE'];
        }

        return $responseData;
    }

    private static function generateCardHash($cardNumber, $cardPassword, $dateTime) {
        return base64_encode(hash("sha512", "{$cardNumber}{$cardPassword}" . $dateTime->format("YmdHisv"), true));
    }

    public function authenticateUser($cardNumber, $cardPassword) {
        $currentDateTime = new \DateTime();

        $data = [
            "CRC" => 0,
            "DATE" => $currentDateTime->format('Y-m-d\TH:i:s\.v\Z'),
            "HASH" => self::generateCardHash($cardNumber, $cardPassword, $currentDateTime),
            "NUMERO_CARTE_FIDELITE" => $cardNumber,
            "LISTE_REFERENTIEL" => [
                "CARTE_FIDELITE_V2" => [
                    "CRC" => 0,
                    "ENVOYER_DONNEES" => true
                ]
            ]
        ];

        $responseData = $this->post("/mobile/api/MonCompte/EnregistrerCarteFideliteV2", $data, true);

        $this->cardNumber = $cardNumber;

        return $responseData;
    }

    public function getReceipts() {
        $data = [
            "CRC" => 0
        ];

        return $this->post("/mobile/api/TicketsFactures/Lister", $data, true);
    }

    public function getReceipt($receiptId, $receiptDate, $shopNumber) {
        $data = [
            "CRC" => 0,
            "DATE" => $receiptDate,
            "ID" => $receiptId,
            "MAGASIN" => $shopNumber
        ];

        return $this->post("/mobile/api/TicketsFactures/Detail", $data, true);
    }
}