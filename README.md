# Requirements

* PHP >= 7
* [Guzzle](https://docs.guzzlephp.org/en/stable/)

# Usage

```php
<?php

require_once("vendor/autoload.php");
require_once("MonELeclercApiClient.php");

$apiClient = new MonELeclercApiClient();

$shopNumber = "0033";
$deviceUuid = MonELeclercApiClient::generateUuid();

$responseData = $apiClient->authenticateDevice($shopNumber, $deviceUuid);

$responseData = $apiClient->authenticateUser("2950972719579", "1234");

$responseData = $apiClient->getReceipts();
```

# Links

* [Mon E.Leclerc](https://www.e-leclerc.com/catalogue/applications-mobiles/mon-eleclerc)
* [Mon E.Leclerc - 1101011.xyz](https://1101011.xyz/com.infomil.bill/index.html)